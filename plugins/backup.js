const { TextEncoder, TextDecoder } = require('util')

shellPlugin.init = init

exports.client = function client(primus, options) {
  Primus.shellPlugin(primus, options, true)
}

exports.server = function server(primus, options) {
  shellPlugin(primus, options, false)
}

exports.library = 'Primus.shellPlugin = ' + shellPlugin.toString()
  + '\n\n' + 'Primus.shellPlugin.init = ' + init.toString()

function shellPlugin(primus, options, isClient) {
  if (shellPlugin.initialized !== true) {
    shellPlugin.init(primus, options, isClient)
  }

  const { incoming, outgoing } = shellPlugin

  // Register our transformers upon each connection
  primus.transform('incoming', incoming)
  primus.transform('outgoing', outgoing)
}

function init(primus, options, isClient) {
  const plugin = this
  // Constants used by our parser and prototype methods
  const SHELL_MSG_TYPE_DATA   = 0
  const SHELL_MSG_TYPE_RESIZE = 1
  const SHELL_MSG_TYPE_EXIT   = 2
  const SHELL_MSG_TYPE_KILL   = 3

  if (isClient === false) {
    const { Spark } = primus
    // Adjust the constructor name to indicate our changes
    ensureProperty(Spark, 'name', 'Shell' + Spark.name)
    extend(Spark.prototype)
  }
  else {
    // Same changes but for the client
    ensureProperty(Primus, 'name', 'Shell' + Primus.name)
    extend(Primus.prototype)
  }

  Object.assign(plugin, { incoming, outgoing })
  plugin.initialized = true

  ///////////////////////////////////////////////////////////////////
  ///////////////////////////  Functions  ///////////////////////////
  ///////////////////////////////////////////////////////////////////

  const parser = {
    [  SHELL_MSG_TYPE_DATA  ](connection, packet, data) {
      data = data.slice(1)

      connection.emit('shell:data', packet.data = data)
    },
    [ SHELL_MSG_TYPE_RESIZE ](connection, packet, data) {
      const cols = data[1] | (data[2] << 8)
      const rows = data[3] | (data[4] << 8)

      connection.emit('shell:resize', cols, rows)
      return false
    },
    [  SHELL_MSG_TYPE_EXIT  ](connection, packet, data) {
      const code = data[1] | (data[2] << 8)

      connection.emit('shell:exit', code)
      return false
    },
    [  SHELL_MSG_TYPE_KILL  ](connection, packet, data) {
      const reason = data.slice(1).toString()

      connection.emit('shell:kill', reason)
      return false
    }
  }

  // Define our message transformers
  function incoming(packet) {
    const { [packet.data[0]]: method } = parser

    if (method !== undefined) {
      return method(this, packet, packet.data)
    }
  }

  function outgoing(packet) {
    packet.data = Buffer.from(packet.data)
  }

  // Used to extend Primus and Spark prototypes
  function extend(prototype) {
    const { write } = prototype

    ensureProperty(prototype, 'write', function shellWrite(data) {
      if (typeof data === 'string') {
        data = encoder(data)
      }

      const msg = new Uint8Array(1 + data.byteLength)

      msg[0] = SHELL_MSG_TYPE_DATA

      msg.set(data, 1)

      return write.call(this, msg.buffer)
    })

    ensureProperty(prototype, 'resize', function shellResize(cols, rows) {
      const msg = new Uint8Array(5)

      msg[0] = SHELL_MSG_TYPE_RESIZE

      msg[1] = cols & 0xFF
      msg[2] = cols >> 8
      msg[3] = rows & 0xFF
      msg[4] = rows >> 8

      return write.call(this, msg.buffer)
    })

    ensureProperty(prototype, 'exit', function shellExit(code) {
      const msg = new Uint8Array(3)

      msg[0] = SHELL_MSG_TYPE_EXIT

      msg[1] = code & 0xFF
      msg[2] = code >> 8

      return write.call(this, msg.buffer)
    })

    ensureProperty(prototype, 'kill', function shellKill(reason) {
      if (typeof data === 'string') {
        data = encoder(reason)
      }

      const msg  = new Uint8Array(1 + data.byteLength)

      msg[0] = SHELL_MSG_TYPE_KILL

      msg.set(data, 1)

      return write.call(this, msg.buffer)
    })

    return prototype
  }

  // Ensure our properties are defined with a similar descriptor or throw
  function ensureProperty(target, key, value) {
    Object.defineProperty(target, key, {
      configurable: true,
      enumerable: true,
      writable: true,
      // Override the above values with the ones from the descriptor
      ...Object.getOwnPropertyDescriptor(target, key),
      // Override the value from the descriptor
      value
    })
  }
}
