import { PassThrough } from 'stream'

export { plugin as server }

let initialized = false

ensureProperty(plugin, 'name', 'plugin [as backpressure-support]')

function plugin(primus, options) {
  if (initialized !== true) {
    init(primus, options)
  }
}

function init(primus, options) {
  const { prototype } = primus.Spark

  initialized = true

  // WeakMap accessor
  const pass = (() => {
    const passthroughs = new WeakMap()

    return stream =>
      passthroughs.has(stream) ?
      passthroughs.get(stream) :
      stream.pipe(passthroughs
        .set(stream, new PassThrough({ allowHalfOpen: false }))
        .get(stream));
  })()

  ensureProperty(prototype, 'pipe', function pipe(stream) {
    pass(this).pipe(stream)

    return this
  })

  ensureProperty(prototype, 'unpipe', function unpipe(stream) {
    pass(this).unpipe(stream)

    return this
  })
}

// Ensure our properties are defined with a similar descriptor or throw
function ensureProperty(target, key, value) {
  Object.defineProperty(target, key, {
    configurable: true,
    enumerable: false,
    writable: false,
    // Override the defaults with the ones from the descriptor
    ...Object.getOwnPropertyDescriptor(target, key),
    // Override the value
    value
  })
}
