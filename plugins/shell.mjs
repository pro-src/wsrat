export function client(primus, options) {
  Primus.shellPlugin(primus, options, true)
}

export function server(primus, options) {
  shellPlugin(primus, options, false)
}

export const library = 'Primus.shellPlugin = ' + shellPlugin.toString()
  + '\n\n' + 'Primus.shellPlugin.init = ' + init.toString()

shellPlugin.init = init

function shellPlugin(primus, options, isClient) {
  if (shellPlugin.initialized !== true) {
    shellPlugin.init(primus, options, isClient)
  }

  // Register our transformer upon each connection
  primus.transform('incoming', shellPlugin.incoming)
}

function init(primus, options, isClient) {
  const plugin = this
  // Constants used by our parser and prototype methods
  const SHELL_MSG_TYPE_DATA   = '\u0000'
  const SHELL_MSG_TYPE_RESIZE = '\u0001'
  const SHELL_MSG_TYPE_EXIT   = '\u0002'
  const SHELL_MSG_TYPE_KILL   = '\u0003'

  if (isClient === false) {
    const { Spark } = primus
    // Adjust the constructor name to indicate our changes
    ensureProperty(Spark, 'name', 'Shell' + Spark.name)
    extend(Spark.prototype)
  }
  else {
    // Same changes but for the client
    ensureProperty(Primus, 'name', 'Shell' + Primus.name)
    extend(Primus.prototype)
  }

  plugin.incoming = incoming
  plugin.initialized = true

  ///////////////////////////////////////////////////////////////////
  ///////////////////////////  Functions  ///////////////////////////
  ///////////////////////////////////////////////////////////////////

  const parser = {
    [  SHELL_MSG_TYPE_DATA  ](connection, packet, data) {
      packet.data = data.slice(1)

      connection.emit('shell:data', packet.data)
    },
    [ SHELL_MSG_TYPE_RESIZE ](connection, packet, data) {
      const cols = data.codePointAt(1)
      const rows = data.codePointAt(2)

      connection.emit('shell:resize', cols, rows)
      return false
    },
    [  SHELL_MSG_TYPE_EXIT  ](connection, packet, data) {
      const code = data.codePointAt(1)

      connection.emit('shell:exit', code)
      return false
    },
    [  SHELL_MSG_TYPE_KILL  ](connection, packet, data) {
      const reason = data.slice(1)

      connection.emit('shell:kill', reason)
      return false
    }
  }

  // Define our message transformer
  function incoming(packet) {
    const { data } = packet

    const method = parser[ data[0] ]

    if (method !== undefined) {
      return method(this, packet, data)
    }
  }

  // Used to extend Primus and Spark prototypes
  function extend(prototype) {
    const { write } = prototype

    ensureProperty(prototype, 'write', function shellWrite(data) {
      return write.call(this, SHELL_MSG_TYPE_DATA + data)
    })

    ensureProperty(prototype, 'resize', function shellResize(cols, rows) {
      return write.call(this, SHELL_MSG_TYPE_RESIZE
        + String.fromCharCode(cols, rows))
    })

    ensureProperty(prototype, 'exit', function shellExit(code) {
      return write.call(this, SHELL_MSG_TYPE_EXIT
        + String.fromCharCode(code))
    })

    ensureProperty(prototype, 'kill', function shellKill(reason) {
      return write.call(this, SHELL_MSG_TYPE_KILL + reason)
    })
  }

  // Ensure our properties are defined with a similar descriptor or throw
  function ensureProperty(target, key, value) {
    Object.defineProperty(target, key, {
      configurable: true,
      enumerable: true,
      writable: true,
      // Override the above values with the ones from the descriptor
      ...Object.getOwnPropertyDescriptor(target, key),
      // Override the value from the descriptor
      value
    })
  }
}
