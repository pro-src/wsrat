import * as shell from './plugins/shell.mjs'
import * as bps from './plugins/backpressure-support.mjs'

export default {
  url: `http://localhost:${process.env.PORT || 1337}/`,
  primus: {
    transformer: 'faye',
    parser: { encoder: noop, decoder: noop },
    plugin: {
      shell,
      'backpressure-support': bps
    }
  },
  shell: 'bash'
}

// We're only using buffers so parser is essentially a noop
function noop(data, cb) {
  cb(null, data)
}
