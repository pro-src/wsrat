#!/bin/bash

set -xe

which git && git --version

# Add upstream remote if missing
git remote -v | grep upstream || \
git remote add upstream https://github.com/pro-src/wsrat.git

set +x
BRANCH=$(git branch | grep \* | cut -d ' ' -f2)
APP_NAME="${1:-${BRANCH}}"
USER=$(git config --global --get user.name) || USER=$(whoami)

echo -e "\n\033[0;32mSup ${USER}, don't forget to"\
        "star this repository!\033[0m\n"
set -x

which heroku || npm install -g heroku

heroku --version
heroku auth:token || heroku auth:login
heroku apps:create "${APP_NAME}"
heroku git:remote -a "${APP_NAME}"

if [[ " $(git branch --list) " =~ .*\ ${APP_NAME}\ .* ]]; then
  PUSH_HEROKU="git push heroku ${APP_NAME}"
else
  PUSH_HEROKU="git push heroku"
fi

$PUSH_HEROKU

set +x
echo -e "\nUse \"\033[0;32m${PUSH_HEROKU}\033[0m\" to push an update"
