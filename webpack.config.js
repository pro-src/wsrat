import HtmlWebpackPlugin from 'html-webpack-plugin'
import HtmlWebpackTagsPlugin from 'html-webpack-tags-plugin'
import HtmlWebpackBeautifyPlugin from 'html-beautify-webpack-plugin'
import MiniCssExtractPlugin from "mini-css-extract-plugin"

import fs from 'fs'
import { resolve } from 'path'
import pkg from './package'

const { NODE_ENV: env } = process.env

const mode = env && /production/i.test(env) ? 'production' : 'development'

module.exports = {
  mode: mode,
  entry: {
    client: ['whatwg-fetch', './browser/client.mjs']
  },
  output: {
    path: resolve(__dirname, 'public'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  optimization: {
    splitChunks: {
//      chunks: 'initial',
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true
        }
      }
    }
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new HtmlWebpackPlugin({
      title: pkg.name,
      meta: {
        viewport: 'width=device-width, initial-scale=1'
      },
      hash: mode === 'development'
    }),
    new HtmlWebpackTagsPlugin({
      scripts: [ 'primus/primus.js' ],
      append: false
    }),
    new HtmlWebpackBeautifyPlugin({
      config: {
        html: {
          end_with_newline: true,
          indent_size: 2,
          indent_with_tabs: false,
          indent_inner_html: true,
          preserve_newlines: true,
          unformatted: [ 'p', 'i', 'b', 'span' ]
        }
      },
      replace: [ ' type="text/javascript"' ]
    })
  ]
}
