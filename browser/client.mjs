import TraceKit from 'tracekit'
import xterm from 'xterm'
import fit from 'xterm/lib/addons/fit/fit'
import screenfull from 'screenfull'
import humane from 'humane-js'
import prettyjson from 'prettyjson'

// Import styles
import 'normalize.css/normalize.css'
import 'xterm/dist/xterm.css'
import 'humane-js/themes/libnotify.css'
import './style.css'

const { Terminal } = xterm

let libnotify

init()

async function main() {
  Terminal.applyAddon(fit)

  const xterm = new Terminal({
    cursorBlink: true
  })

  const container = document.createElement('div')
  container.setAttribute('id', 'terminal-container')

  document.body.appendChild(container)

  xterm.open(container)

  screenfull.on('error', TraceKit.report)

  xterm.element.addEventListener('dblclick', event => {
    screenfull.toggle(document.body)
  })

  xterm.fit()
  xterm.focus()

  const primus = new Primus('http://localhost:1337/?shell=bash')

  xterm.on('data', data => primus.write(data))
  xterm.on('error', TraceKit.report)

  primus.once('open', () => libnotify.success('Connected'))
  primus.on('data', data => xterm.write(data))
  primus.on('error', TraceKit.report)
  primus.once('end', () => libnotify.info('Disconnected'))
  primus.on('reconnect timeout', (err, opts) =>
    libnotify.error(`Timeout expired: ${err.message}`))

  primus.on('reconnect scheduled', function timer(opts) {
    timer.remaining = parseInt(opts.scheduled / 1000)

    if (('id' in timer) === false || timer.id === -1) {
      timer.id = setInterval(() => {
        let msg = 'Reconnecting...'

        if (1 < timer.remaining--) {
          msg = `Reconnect attempt in ${timer.remaining} seconds`
        }

        if (/reconnect/i.test(libnotify.el.textContent)) {
          libnotify.el.innerHTML = `<ul><li>${msg}<ul><li>`
        }
      }, 1000)

      if (libnotify.currentTimer) {
        setTimeout(start, 2000)
      }
      else {
        start()
      }

      function start() {
        if (timer.id !== -1) {
          libnotify.log('Reconnecting...', { timeout: 300000 })
        }
      }

      primus.once('reconnecting', () => {
        if (timer.id !== -1) {
          clearInterval(timer.id)
          timer.id = -1
        }
      })
    }
  })

  primus.on('reconnect failed', (err, opts) =>
    libnotify.error(`The reconnection failed: ${err.message}`))
  primus.on('reconnected', opts =>
    libnotify.success(`Reconnected in ${opts.duration} ms`))

  window.addEventListener('resize', () => {
    xterm.fit()
    primus.resize(xterm.cols, xterm.rows)
  })
}

// Setup the humane alerts
function humanize() {
  try {
    libnotify = humane.create({ baseCls: 'humane-libnotify' })

    const spawn = (type) =>
      libnotify.spawn({ addnCls: `humane-libnotify-${type}` })

    libnotify.info    = spawn('info')
    libnotify.success = spawn('success')
    libnotify.error   = spawn('error')
  }
  catch(error) {
    alert(error.stack)
  }
}

function init() {
  humanize()

  try {
    TraceKit.report.subscribe(function(error) {
      alert(prettyjson.render(error || `falsy error: ${error}`))
    })

    TraceKit.extendToAsynchronousCallbacks()
  }
  catch(error) {
    alert(`Failed to setup tracekit: ${error.stack}`)
  }

  main().catch(TraceKit.report)
}
