import express from 'express'
import http from 'http'
import Primus from 'primus'

import turn from '../lib/turn.mjs'

export default function main({ primus: options, port }) {
  const app = express()
  const server = http.createServer(app)

  if (options.plugin && ('shell' in options.plugin)) {
    delete options.plugin.shell
  }

  const primus = new Primus(server, options)

  turn(primus)

  server.listen(port || 1337)
}
