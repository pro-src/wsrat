import Primus from 'primus'
import pty from 'node-pty'
import path from 'path'
import repl from 'repl'

export default function main({ primus: options, shell, url }) {
  const Socket = Primus.createSocket(options)

  terminal(url + '?type=server', shell)

  function connect(uri, callback) {
    const spark = new Socket(uri)

    spark.on('open', function open() {
      callback(spark)
    })

    spark.on('error', console.error)

    spark.on('end', function end() {
      setTimeout(terminal, 200, uri, options.shell)
    })
  }

  function pipe(spark, ...args) {
    while (args.length < 2) args.push(new PassThrough())

    const [ input, output ] = args

    // Can't use pipe here: primus@7.1.1 and node-pty@0.7.4
    output.pipe(spark)

    return { input, output }
  }

  function terminal(uri, filename = 'bash', args) {
    const { name } = path.parse(filename)

    if (filename === 'repl') {
      connect(`${uri}&shell=repl`, spark => {
        const { input, output } = pipe(spark)

        const r = repl.start({ input, output, terminal: true })

        r.defineCommand('terminal', str => {
          terminal(uri, ...str.split('\x20'))
        })

        setup(spark, r)

        r.context.spark = spark
      })
    }
    else {
      connect(`${uri}&shell=${name}`, spark => {
        const shell = pty.spawn(filename, args, { encoding: null })

        spark.on('shell:resize', (cols, rows) =>
          shell.resize(cols, rows))

        setup(spark, shell)
        pipe(spark, shell, shell)
      })
    }
  }

  function setup(spark, shell) {
    shell.on('exit', function exit() {
      spark.end()
    })

    shell.on('error', error => console.error(error))
  }
}
