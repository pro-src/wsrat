import express from 'express'
import http from 'http'
import Primus from 'primus'

import terminal from '../lib/terminal.mjs'

import { fileURLToPath } from 'url';
import { dirname, join } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export default function main({ primus: options, shell, port }) {
  const app = express()
  const server = http.createServer(app)
  const primus = new Primus(server, options)

  app.use(express.static(join(__dirname, '../public')))

  primus.on('connection', function(spark) {
    terminal(spark, spark.query.shell || shell)
  })

  server.listen(port || 1337)
}
