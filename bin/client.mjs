import Primus from 'primus'

export default function main({ primus: options, shell, url }) {
  const Socket = Primus.createSocket(options)
  const client = new Socket(`${url}?shell=${shell}`)

  process.stdin.pipe(client)
  client.pipe(process.stdout)

  process.stdout.on('resize', function() {
    client.resize(process.stdout.columns, process.stdout.rows)
  })

  client.on('open', function() {
    // Deferred so you may ^C interrupt if something breaks in advance
    process.stdin.setRawMode(true)
  })

  client.on('error', console.error)

  client.on('shell:exit', function(code) {
    process.exit(code)
  })

  client.on('shell:kill', function(reason, code) {
    console.log('client killed:', reason)
    process.exit(code)
  })

  client.on('end', function() {
    process.exit(1)
  })
}
