import primus from 'primus'
import { PassThrough } from 'stream'
import logger from 'debug'

const { Spark } = primus

const debug = logger('peer')

const bindings = new WeakMap()

// This class wraps Primus.Spark instances to add backpressure support and
// custom bind/unbind methods
export class Peer extends PassThrough {
  constructor(spark, options) {
    options = { allowHalfOpen: false, ...options }

    super(options)

    if (!(spark instanceof Spark)) {
      throw new Error('Inalid argument, args[0] must be a Spark instance')
    }

    debug('connection: %s', spark.id)

    Object.defineProperty(this, 'spark', { value: spark })

    spark.pipe(this)
    spark.on('error', this.emit.bind(this, 'error'))

    if (options.allowHalfOpen) {
      spark.prependListener('end', () => {
        if (bindings.has(this)) {
          this.unbind()
        }
      })
    }
  }
  bind(target, options) {
    debug('bind %s to %s', this, target)

    duplex = arguments.length > 2 ? duplex : !this.allowHalfOpen

    if (!(target instanceof Spark)) {
      throw new TypeError('Invalid argument, args[0] must be a Spark instance.')
    }

    if (bindings.has(this)) {
      return bindings.get(this) === target
    }

    if (duplex ? target.bind(this, options, false) : true) {
      this.passThrough.pipe(target, options)

      debug('pipe %s to %s', this, target)
      bindings.set(this, target)
      return target
    }

    throw new Error('unable to bind: ' + target)
  }
  unbind(target, duplex) {
    debug('unbind %s from %s', this, target)

    duplex = arguments.length > 1 ? duplex : !this.allowHalfOpen

    if (arguments.length > 0 && !(target instanceof Peer)) {
      throw new TypeError('Invalid target, target must be a Peer instance.')
    }

    if (bindings.has(this) === false) return true

    const bound = bindings.get(this)

    if (target !== undefined && target !== bound) return false

    if (duplex ? bound.unbind(this, false) : true) {
      this.unpipe(bound.spark)

      debug('unpipe %s from %s', this, bound)
      if (bindings.delete(this)) {
        return target
      }
    }

    throw new Error('unable to unbind: ' + bound)
  }
  get id() {
    return this.spark.id
  }
  isBound() {
    return bindings.get(this)
  }
  toString() {
    return `[Peer: ${this.id}]`
  }
}
