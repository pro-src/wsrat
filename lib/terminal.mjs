import Ultron from 'ultron'
import pty from 'node-pty'
import { PassThrough } from 'stream'

export default function terminal(spark, filename = 'bash', args, options) {
  const term = pty.spawn(filename, args, { encoding: 'utf-8', ...options })

  const uTerm = new Ultron(term)
  uTerm.alive = true

  const uSpark = new Ultron(spark)
  uSpark.alive = true

  function destroy(ultron) {
    uTerm.remove()
    uSpark.remove()

    if (ultron instanceof Ultron && ultron.alive === true) {
      ultron.ee.end()
    }
  }

  function onend(ultronA, ultronB, name) {
    return function end() {
      console.log(name, 'ended')
      ultronA.alive = false
      if (name !== 'term')
      destroy(ultronB)
    }
  }

  // term <-> spark
  uTerm.on('data', data => spark.write(data)) // >>>> term -> spark
  uSpark.on('data', data => term.write(data)) // <<<< term <- spark
//  spark.pipe(term)
//  term.pipe(spark)

  uSpark.on('shell:resize', function(cols, rows) {
    console.log('resize:', cols, rows)
    term.resize(cols, rows)
  })

  uSpark.on('shell:kill', reason => {
    destroy(uTerm)

    console.log('killed:', reason)
  })

  uTerm.on('end', onend(uTerm, uSpark, 'term'))
  uSpark.on('end', onend(uSpark, uTerm, 'spark'))

  uTerm.on('exit', function exit(code) {
    console.log('term exit')
//    destroy(false)

    spark.exit(code)
  })

  uTerm.on('error', function(error) {
    console.error(error)
  })
}
